let React = require('react');

let Products = React.createClass({
    getInitialState: () => {
        return {
            products: []
        }
  },
  handleAPICall: () => {
      fetch('http://localhost:3001/products/all')
      .then((response) => {
        return response.json()
      })
      .then((products) => {
        this.setState({ products: products })
      })
  },
  componentDidMount: () => {
    this.handleAPICall();
  },
  render() {
    const products = this.state.products.map((item, i) => {
      return <div>
        <span>{item._id} / {item.name} / {item.quantity} / {item.price}</span>
      </div>
    });

    return <div>
      <div className="products">{ products }</div>
    </div>
  }
  });

module.exports = Products;