let React = require('react');
let ReactDOM = require('react-dom');
let Products = require('Products');

ReactDOM.render(
  <Products/>,
  document.getElementById('app')
);
